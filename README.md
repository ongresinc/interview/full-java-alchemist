# Full Java Alchemist

## What do we expect?
We expect that the amount of effort to do any of these exercises is in  a few hours of actual work. We also understand that your time is valuable, and in anyone's busy schedule that constitutes a fairly substantial chunk of time, so we really appreciate any effort you put into helping us build a solid team.

## What we are looking for?
Keep it simple. Really. We really don't want you spending too much more time on it.

> **Treat it like production code.** But Keep it simple.

That is, develop your software in the same way that you would for any code that is intended to be deployed to production. These may be toy exercises, but we really would like to get an idea of how you build code on a day-to-day basis.
  
## How to submit?
You can do this however you see fit - you can email us a tarball, a pointer to download your code from somewhere or just a link to a source control repository. Make sure your submission includes a small **README**, documenting any assumptions, simplifications and/or choices you made, as well as a short description of how to run the code and tests. Finally, to help us review your code, please split your commit history in sensible chunks (at least separate the initially provided code from your personal additions).

## The Interview
After you submit your code, we will contact you to discuss and arrange an in-person (if it's possible) interview  with some of the team. The interview will cover a wide range of technical and social aspects relevant to working at OnGres, but importantly for this exercise: we will also take the opportunity to step through your submitted code with you.

## The Exercise

### Part 1

#### Word wrap

You have to write a class called [Wrapper](src/main/java/com/ongres/wrapper/Wrapper.java), that has a single static function named wrap that takes two arguments, a String, and a column width number. 

The function returns the string, but with line breaks inserted at just the right places to make sure that no line is longer than the column width. You try to break lines at word boundaries, but if it's necessary you can break a word. 

Like a word processor, break the line by replacing the last space in a line with a newline.

You have the Unit Tests in the class [WrapperTest](src/test/java/com/ongres/WrapperTest.java)

### Part 2

The [Sakila](https://www.jooq.org/sakila) database example model a DVD rental store, and we want to play with it.

Check the [Use of Docker image](#use-of-docker-image) section to have a basic understanding of running it.

The main idea is to create a tool to manage a hypothetical DVD Rental Store, and you can implement it with a REST API. 

- **Requirements:**

  * You can use any framework you prefer to build REST API, here are some examples: Spring Boot, Micronaut, Dropwizard (JAX-RS), ...
  * The response must be a JSON.
  * To read from the database you should use “pure” JDBC, without frameworks this time.

- **Reports** 

  We would like to generate reports using the API REST with the following information:

  * Number of Clients by the requested Country (and optionally by City too)
  * Films by requested Actor, with a filter by Category.
    * Return the following information:  Actor First Name, Actor Last Name, Film Title, Description, Category Name. 
    * The filter by category should not be in the query (hint: you can use the Java Stream API).

- **Find Overdue DVDs**

    Many DVD stores produce a daily list of overdue rentals so that customers can be contacted and asked to return their overdue DVDs.

    To create such a list, search the rental table for films with a return date that is NULL and where the rental date is further in the past than the rental duration specified in the film table. If so, the film is overdue and we should produce the name of the film along with the customer name and phone number. For example, the query should return (in the correct API format):

    ```
        +------------------+--------------+------------------+
        | customer         | phone        | title            |
        +------------------+--------------+------------------+
        | OLVERA, DWAYNE   | 62127829280  | ACADEMY DINOSAUR |
        | HUEY, BRANDON    | 99883471275  | ACE GOLDFINGER   |
        | BROWN, ELIZABETH | 10655648674  | AFFAIR PREJUDICE |
        | OWENS, CARMEN    | 272234298332 | AFFAIR PREJUDICE |
        | HANNON, SETH     | 864392582257 | AFRICAN EGG      |
        +------------------+--------------+------------------+
    ```


### Use of Docker image
We like Docker, so we use it (almost) everywere, this test is no exception :wink:. If you don't know how to use docker don't worry, only a few basic steps are required to be up and running.

This exercise use a docker image with PostgreSQL 11 and the [Sakila](https://www.jooq.org/sakila) database loaded (check the link to see the schema). The user, password and database name is `sakila`.

The project contains the `Dockerfile` to build the image in the docker folder, so you can build it running `docker build -t sakila-img .` inside that folder.

The generated image name is `sakila-img`, simply run `docker run -d -p 5432:5432 --name sakila-pg sakila-img` to create a container named `sakila-pg`.
Now you can connect to the PostgreSQL database with `psql -h localhost -p 5432 sakila sakila` or using the IP of the Docker container, depends on the operative system. 


## F.A.Q.

*Do you have to complete all the tests?* Ideally yes, but don’t worry if you don’t have enough time to finish all the exercises. We’re going to value the clean code and the correctness of the solution.

*Is it OK to share your solutions publicly?* Yes, the questions are not prescriptive, the process and discussion around the code is the valuable part. You do the work, you own the code. Given we are asking you to give up your time, it is entirely reasonable for you to keep and use your solution as you see fit.

*Should I do X?* For any value of X, it is up to you, we will leave it up to you to provide us with what you see as important. Just remember the rough time frame of the project. If it is going to take you a couple of days, it isn't essential.

*Something is ambiguous, and I don't know what to do?* The first thing is: don't get stuck. We really don't want to trip you up intentionally, we are just attempting to see how you approach problems. If you really feel stuck, our first preference is for you to make a decision and document it with your submission. If you feel it is not possible to do this, just send us an email and we will try to clarify or correct the question for you.

### **Good luck!**
