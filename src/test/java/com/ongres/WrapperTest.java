package com.ongres;

import static com.ongres.wrapper.Wrapper.wrap;
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

public class WrapperTest {

  @Test
  public void emptyString() {
    assertEquals("", wrap("", 1));
  }

  @Test
  public void stringShorterThanCol() {
    assertEquals("this", wrap("this", 10));
  }

  @Test
  public void wrapTwoWordsAfterSpace()  {
    assertEquals("word\nword", wrap("word word", 6));
  }

  @Test
  public void wrapThreeWordsAfterFirstSpace() {
    assertEquals("word\nword\nword", wrap("word word word", 6));
  }

  @Test
  public void wrapThreeWordsAfterSecondSpace() {
    assertEquals("word word\nword", wrap("word word word", 11));
  }

  @Test
  public void splitOneWord() {
    assertEquals("wo\nrd", wrap("word", 2));
  }

  @Test
  public void splitOneWordManyTimes() {
    assertEquals("abc\ndef\nghi\nj", wrap("abcdefghij", 3));
  }

  @Test
  public void wrapOnWordBoundary() {
    assertEquals("word\nword", wrap("word word", 5));
  }

  @Test
  public void wrapAfterWordBoundary() {
    assertEquals("word\nword", wrap("word word", 6));
  }

  @Test
  public void wrapWellBeforeWordBoundary() {
    assertEquals("wor\nd\nwor\nd", wrap("word word", 3));
  }

  @Test
  public void wrapJustBeforeWordBoundary() {
    assertEquals("word\nword", wrap("word word", 4));
  }
}
